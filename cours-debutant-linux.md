<!--
author:   Yanis Cernesse

email:    LiaScript@web.de

version:  0.0.2

language: fr

narrator: French Male


-->

# Cours Linux débutant


__Ouvrir le cours:__

https://liascript.github.io/course/?https://framagit.org/solutrucs/testliascript/-/raw/main/cours-debutant-linux.md

## Linux c'est quoi ?

Linux est un système d'exploitation Open Source et libre.

Libre d'être modifié et utilisé gratuitement.

Actuelement linux est quasiment compatible avec tout ce qui ressemble de pret ou de loin a un ordinateur.

Il existe des distributions c'est a dire un assemblages de logiciels et de morceaux de linux pour quasiment tout les usages.

Quelques utilisations courantes de linux et la distribution utilisé:

- Linux est installé sur 3,17% des ordinateurs du monde.
- 95% des serveurs du monde fonctionne sous linux. ( Ubuntu, RedHat, Debian, Fedora, Centos)
- Les 500 supercomputers les plus puissants sont sous linux.
- 90% des infra cloud sont sous Linux
- 85% des smartphone sont basé sur Linux.
- Tous les projets spaciaux tourne sur Linux.
- 90% des éffets spéciaux a Hollywood sont fait sur Linux.
- Les consoles tourne soit sur un systeme Unix soit sur Linux ( PS4, PS3 ).
- Seul les ordinateurs de bureau sont majoritairement windows.
- L'embarqué tourne a 37% sous linux.
- Les mainframe 30% de linux le reste de Unix propiétaire.


## Pourquoi Linux ?

Car il est libre gratuit et modifiable a volonté et surtout léger contrairement a ces concurents.

## Les concepts clées

### Sur Linux tout est un fichier

Sur linux tout est un fichier donc toute modification est stocké dans un fichier qu'on peut ouvrir avec un éditeur texte ( ou présque ;-) )

**L'arborésence de Linux et ses dossiers importants:**

-  `/bin` - applications binaires
-  `/boot` - fichiers de configuration du démarrage (boot), noyaux et d'autres fichiers indispensables au moment du démarrage (boot).
-  `/dev` - fichiers de périphériques (devices)
-  `/etc` - fichiers de configuration, scripts de démarrage, etc.
-  `/home` - répertoires personnels des différents utilisateurs (home = maison)
-  `/initrd `- utilisé lors de la création du processus de démarrage initrd personnalisé
-  `/lib` - bibliothèques (libraries) système en `.so` équivalent des `.dll` de windows
-  `/media` - partitions montées automatiquement sur votre disque dur, CDs, clés USB, etc.
-  `/mnt` - systèmes de fichiers montés manuellement sur votre disque dur
-  `/opt` - fournit un emplacement pour des applications optionnelles (tierces) pouvant y être installées souvent des applications propiétaires
-  `/root` - répertoire personnel du super-utilisateur (root), prononcé « slash-root »
-  `/sbin` - binaires système importants
-  `/tmp` - fichiers temporaires
-  `/usr` - applications et fichiers généralement accessibles à tous les utilisateurs (users)
-  `/var` - fichiers variables tels que journaux et bases de données


### Le concept des entrés et sorties standard

Quand t'on travaille dans un terminal un programme peut souvent prendre quelquechose en éntre et sortir quelquechose il faut voir ca comme une usine.

_**Ici la sortie par défaut est souvent la console**_

<!-- style="display: block; margin-left: auto; margin-right: auto; max-width: 315px;" -->
```    ascii
                                             +-----------+
                         +-----------++----> |  Console  |
                         |           |       +-----------+
                         |           |
+----------+             |           |       +-----------+
|  Entrée  | +-------->  | Commande  |+----> |  Fichier  |
+----------+             |           |       +-----------+
                         |           |
                         |           |       +------------------+
                         +-----------++----> |  Autre commande  |
                                             +------------------+
```

On le vera plus tard mais on utilise souvent cette possibilité cela permet d'enchainer des commandes qui si elles savent juste travailler avec une


## La ligne de commande

Sous les serveurs linux on utilise souvent la ligne de commande c'est souvent plus rapide plus éfficace et surtout automatisable simplement. On va voir comment l'utiliser bientot.

### Case sensitif

Sous linux il faut faire attention a la "case" ce qui veut dire que le fichier suivant:
`MonfichierTropBien.txt` est différent des fichiers suivants:

- `monfichiertropbien.txt`

- `MonfichierTropBien`

- `Monfichiertropbien.txt`

- `MonfichierTropBien.TXT`

### SSH

### Lance tes prémieres commandes

#### La commande echo

<!--
data-showGutter="false"
data-theme="chaos" -->
```shell  Voici un terminal
[packer@pc-69 ~]$
```

***On va taper notre prémiere commande: echo***

<!-- data-theme="chaos" -->
```shell
echo Salut le monde
```
***On obtient le résultat suivant:***
<!-- data-theme="chaos" -->
```shell
[packer@pc-69 ~]$ echo Salut le monde
Salut le monde
```
Pour l'instant ca ne sert pas à grand chose mais c'est important pour plus tard.


#### La commande whoami

<!-- data-theme="chaos" -->
```shell
whoami
```
***On obtient le résultat suivant:***
<!-- data-theme="chaos" -->
```shell
[packer@pc-69 ~]$ whoami
packer
```
Ca peut être utile quand on sait qu'un programe ne doit tourner qu'avec un utilisateur particulier

#### Quiz


<div>

** Comment fait on pour afficher du texte dans la sortie standard ici l'ecran: **

_(Attention plusieurs solutions)_

    [[X]] `echo Mon texte ici`
    [[ ]] `Mon texte ici`
    [[X]] `echo "Mon texte ici"`
    [[ ]] `Echo Salut le monde`


** Comment fait on pour afficher l'utilisateur avec lequel on est connecté: **

<!-- data-theme="chaos" -->
```shell
[packer@pc-69 ~]$
```

_(Attention plusieurs solutions)_

    [[X]] `c'est affiché a gauche du prompt ici packer`
    [[ ]] `who am i`
    [[X]] `whoami"`

</div>

#### Recapitulatif

| Commande             |   Description |
| -------------------- |:-------------:|
| echo                 | Afficher le texte qu'on veut dans la sortie standard ici l'ecran |
| whoami               | Afficher l'utilisateur avec lequel je suis connécté   |
